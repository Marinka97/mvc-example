import { Images } from '../../assets';
import { gameConfig } from '../../constants/GameConfig';
import BaseScene from './BaseScene';

export default class LobbyScene extends BaseScene {
  public static CLICK_EVENT: string = 'sendClickEvent';
  public static SEND_START_GAME_NOTIFICATION: string = `${
    LobbyScene.NAME
  }SendStartGameNotification`;

  public static NAME: string = 'LobbyScene';
  public text: Phaser.GameObjects.Text;

  private sprite: Phaser.GameObjects.Sprite;
  constructor() {
    super(LobbyScene.NAME);
  }

  public create(): void {
    this.setButtonImage();
    this.setText();
    this.setListeners();
  }

  public setButtonImage(): void {
    this.sprite = this.add.sprite(
      gameConfig.canvasWidth / 2,
      gameConfig.canvasHeight / 2,
      Images.Button.Name,
    );
    this.sprite.setInteractive();
  }

  private setListeners(): void {
    this.sprite.on('pointerup', this.buttonPointerUp, this);
  }

  private buttonPointerUp(): void {
    this.events.emit(LobbyScene.CLICK_EVENT);
  }

  private setText(): void {
    this.text = this.add.text(
      (gameConfig.canvasWidth = 135),
      (gameConfig.canvasHeight = 470),
      'Start Game',
    );
    this.text.setFontSize(45);
    this.text.setColor('#ffc500');
    this.text.setFontStyle('bold');
  }

  // this.scene.LobbyScene.NAME.stop /// start
}
