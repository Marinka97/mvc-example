import BaseScene from './BaseScene';

export default class GameScene extends BaseScene {
  public static NAME: string = 'GameScene';

  public static SEND_CLICK_EVENT: string = 'sendClickEvent';
  public static SEND_CLICK_NOTIFICATION: string = `${
    GameScene.NAME
  }SendClickNotification`;

  constructor() {
    super(GameScene.NAME);
  }

  public preload(): void {
    this.load.image('belle', 'assets/images/belle.png');
  }

  public create(): void {
    this.setImageListener();
  }

  public startGame(text: string): void {
    this.scene.start;
  }

  // public updatePosition

  public setImageListener(): void {
    let sprite = this.add.sprite(150, 200, 'belle').setInteractive();
    sprite.on('pointerup', this.scenePointerUp, this);
  }

  public scenePointerUp() {
    this.events.emit(GameScene.SEND_CLICK_EVENT);
  }
}
