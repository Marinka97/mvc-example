import PlayerVOProxy from '../../model/PlayerVOProxy';
import BaseSceneMediator from './BaseSceneMediator';
import LobbyScene from './LobbyScene';
import PreloadScene from './PreloadScene';

export default class LobbySceneMediator extends BaseSceneMediator<LobbyScene> {
  public static NAME: string = 'LobbySceneMediator';

  constructor() {
    super(LobbySceneMediator.NAME, null);
  }

  public registerNotificationInterests(): void {
    this.subscribeToNotifications(PreloadScene.LOAD_COMPLETE_NOTIFICATION);
  }

  public handleNotification(notificationName: string): void {
    switch (notificationName) {
      case PreloadScene.LOAD_COMPLETE_NOTIFICATION:
        this.game.scene.start(LobbyScene.NAME);
        break;

      default:
        console.warn(`${notificationName} is unhandled!`);
        break;
    }
  }

  protected setView(): void {
    const lobbyScene: LobbyScene = new LobbyScene();
    this.scene.add(LobbyScene.NAME, lobbyScene);
    this.setViewComponent(lobbyScene);
    super.setView();
  }

  protected setViewComponentListeners(): void {
    this.viewComponent.events.on(
      LobbyScene.CLICK_EVENT,
      this.sendClickNotification,
      this,
    );
  }

  private sendClickNotification(): void {
    this.sendNotification(LobbyScene.SEND_START_GAME_NOTIFICATION);
    this.scene.stop(LobbyScene.NAME);
  }

  get playerVOProxy(): PlayerVOProxy {
    return this.facade.retrieveProxy(PlayerVOProxy.NAME);
  }
}
