import { BaseProxy } from './BaseProxy';
import GameVO from './vo/GameVO';

export default class GameVOProxy extends BaseProxy<GameVO> {
  public static NAME: string = 'GameVOProxy';
  public static CHANGE_POSITION: string = `${GameVOProxy.NAME}ChangePosition`;
  constructor() {
    super(GameVOProxy.NAME, new GameVO());
  }

  public changePosition() {
    this.vo.imagePosition; // set random position

    this.sendNotification(GameVOProxy.CHANGE_POSITION);
  }
}
