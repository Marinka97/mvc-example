import { SimpleCommand } from '@planet221b/pure-mvc';
import GameVOProxy from '../../model/GameVOProxy';

export default class changePosition extends SimpleCommand {
  public execute(): void {
    const gameVoProxy: GameVOProxy = this.facade.retrieveProxy(
      GameVOProxy.NAME,
    );
    gameVoProxy.changePosition();
  }
}
