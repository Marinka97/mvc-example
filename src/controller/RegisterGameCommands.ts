import { SimpleCommand, SyncMacroCommand } from '@planet221b/pure-mvc';
import GameScene from '../view/scenes/GameScene';
import changePosition from './game/changePosition';

export default class RegisterGameCommands extends SyncMacroCommand<
  SimpleCommand
> {
  public execute(): void {
    this.facade.registerCommand(
      GameScene.SEND_CLICK_NOTIFICATION,
      changePosition,
    );
  }
}
